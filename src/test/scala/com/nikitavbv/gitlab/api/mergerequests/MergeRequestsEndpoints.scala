package com.nikitavbv.gitlab.api.mergerequests

import com.nikitavbv.gitlab.api.TestConfig
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.response.Response

object MergeRequestsEndpoints {

  def getAllMergeRequests(projectId: Long): Response = `given`().when()
    .get(s"/projects/$projectId/merge_requests")
    .`then`().extract().response()

  def getMergeRequest(projectId: Long, mergeRequest: Long): Response = {
    print(s"/projects/$projectId/merge_requests/$mergeRequest")
    `given`().when()
      .get(s"/projects/$projectId/merge_requests/$mergeRequest")
      .`then`().extract().response()
  }

  def postComment(projectId: Long, mergeRequestId: Long, body: String): Response = `given`().when()
    .param("body", body)
    .post(s"/projects/$projectId/merge_requests/$mergeRequestId/notes")
    .`then`().extract().response()

  def updateMergeRequest(projectId: Long, mergeRequestId: Long, description: String): Response = `given`().when()
    .param("description", description)
    .put(s"/projects/$projectId/merge_requests/$mergeRequestId")
    .`then`().extract().response()

  def deleteComment(projectId: Long, mergeRequestId: Long, commentId: Long): Response = `given`().when()
    .delete(s"/projects/$projectId/merge_requests/$mergeRequestId/notes/$commentId")
    .`then`().extract().response()

  def createToDoItem(projectId: Long, mergeRequestId: Long): Response = `given`().when()
    .post(s"/projects/$projectId/merge_requests/$mergeRequestId/todo")
    .`then`().extract().response()

  def approveMergeRequest(projectId: Long, mergeRequestId: Long): Response = `given`().when()
    .post(s"/projects/$projectId/merge_requests/$mergeRequestId/approve")
    .`then`().extract().response()

  def unapproveMergeRequest(projectId: Long, mergeRequestId: Long): Response = `given`().when()
    .post(s"/projects/$projectId/merge_requests/$mergeRequestId/unapprove")
    .`then`().extract().response()

  private def given() = RestAssured.`given`()
    .baseUri(TestConfig.GITLAB_API_ENDPOINT)
    .header("PRIVATE-TOKEN", TestConfig.PERSONAL_ACCESS_TOKEN)
}
