package com.nikitavbv.gitlab.api.mergerequests

import org.hamcrest.Matchers.{equalTo, is}
import org.scalatest.FunSuite

class MergeRequestsTest extends FunSuite {

  def turbocowProject = 24418897

  test("get all merge requests should return a list of merge requests") {
    MergeRequestsEndpoints.getAllMergeRequests(turbocowProject).`then`().assertThat()
      .statusCode(200)
      .body("size()", is(20))
  }

  test("get specific merge request should return all details") {
    MergeRequestsEndpoints.getMergeRequest(turbocowProject, 22).`then`().assertThat()
      .statusCode(200)
      .body("id", equalTo(95792117))
      .body("title", equalTo("Livestonk: dependency injection framework, which any cow would love to have!"))
      .body("state", equalTo("merged"))
      .body("merged_by.name", equalTo("Nikita Volobuev"))
      .body("target_project_id", equalTo(turbocowProject))
      .body("reference", equalTo("!22"))
  }

  test("posting a comment should create a note successfully") {
    MergeRequestsEndpoints.postComment(24418897, 22, "Looks good to me!").`then`().assertThat()
      .statusCode(201)
      .body("body", equalTo("Looks good to me!"))
      .body("noteable_type", equalTo("MergeRequest"))
  }

  test("updating description of merge request should return updated info") {
    MergeRequestsEndpoints.updateMergeRequest(24418897, 22, "new description").`then`().assertThat()
      .statusCode(200)
      .body("id", equalTo(95792117))
      .body("project_id", equalTo(24418897))
      .body("description", equalTo("new description"))
  }

  test("deleting comment which does not exist should return not found") {
    MergeRequestsEndpoints.deleteComment(24418897, 22, 42).`then`().assertThat()
      .statusCode(404)
  }

  test("creating todo items returns 304, because there is already a todo") {
    MergeRequestsEndpoints.createToDoItem(24418897, 22).`then`().assertThat()
      .statusCode(201)
  }

  test("request cannot be approved if there are no rights to do so") {
    MergeRequestsEndpoints.approveMergeRequest(24418897, 24).`then`().assertThat()
      .statusCode(401)
  }

  test("request cannot be unapproved if there was no approve") {
    MergeRequestsEndpoints.unapproveMergeRequest(24418897, 24).`then`().assertThat()
      .statusCode(404)
  }
}
