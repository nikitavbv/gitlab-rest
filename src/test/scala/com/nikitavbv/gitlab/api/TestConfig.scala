package com.nikitavbv.gitlab.api

object TestConfig {

  def GITLAB_API_ENDPOINT = "https://gitlab.com/api/v4"
  def PERSONAL_ACCESS_TOKEN: String = System.getenv("GITLAB_TESTING_TOKEN")
}