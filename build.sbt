name := "gitlab-rest-api-testing"

version := "0.1"

scalaVersion := "2.13.5"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % Test
libraryDependencies += "io.rest-assured" % "rest-assured" % "4.3.3" % Test